# NodeJS container
FROM node

#Create work directory for docker container
RUN mkdir /flatris
#Go to work directory
WORKDIR /flatris

#Image build optimization
COPY package.json /flatris
RUN yarn install

#Copy code to docker container
COPY . /flatris

#Test and build application
RUN yarn test
RUN yarn build

#Run application
CMD yarn start

#Open port 3000
EXPOSE 3000